# Web pages for [SUT](https://sut.sh.cvut.cz)

## Local development
1. Install dependencies with `pipenv`

```bash
$ pipenv install
$ pipenv shell
```

2. Run `watchdog` for automation of the building process

```bash
$ watchmedo shell-command --command="python build.py" \
                         --patterns="*.html;*.yaml" \
                         templates/ .
```

3. Update `content.yaml` or `templates/*.html`

## Contribution

For updating page content change `content.yaml` and send [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html). After merge to `master` CI will update and publish new webpages.

### Slides

Upload your slides to the `public/slides/<year>`. The name convention for a file is `<YYYYMMDD>-<first_name>_<last_name>-<lecture_name>`. Also update content of `content.yaml`. Find your record and add: 

```yaml
- date: "8. 10. 2019"
  time: "20:30"
  # ...
  slides: "<year>/<file_name>"
  link: "https://example.com" # optional
```
