import sys
import os
import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape

def render_html():
    # Create environment
    env = Environment(loader = FileSystemLoader('templates'),
            autoescape=select_autoescape(['html'])
            )

    with open("content.yaml", 'r') as stream:
        content = yaml.safe_load(stream)

    # Save file
    for template_file in os.listdir('templates'):
        if template_file == "base.html":
            continue

        # Load template
        template = env.get_template(template_file)

        with open(os.path.join('public', template_file), 'w') as file:
            file.write(template.render(content))

        print(f"Template '{template_file}' was successfully rendered.", file=sys.stderr)

def main():
    render_html()

if __name__ == "__main__":
    main()
